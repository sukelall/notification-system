@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
          {{--   <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                </div>
              </div> --}}

              @if(count($posts) != 0 )

              @foreach($posts as $post)
              <div class="panel panel-primary">
                <div class="panel-heading">{{ $post->title }}</div>
                <div class="panel-body">{{ $post->description }}</div>

                <br>

                <br>
                <h3>Comment</h3>


                @if(count($post->comments) != 0)

                
                @foreach($post->comments as $comment)

                <h5>{{ $comment->text }}</h5>
                @endforeach


                @else 

                @endif

                <form method="post" action="{{ route('comment.store') }}">

                  {{ csrf_field() }}

                  <div class="form-group">
                    <textarea name="text" id="text" class="form-control"></textarea>
                  </div>
                  <input type="hidden" value="{{ Auth::user()->id }}" name="user_id" id="user_id">
                  <input type="hidden" value="{{ $post->id }}" name="post_id" id="post_id">

                  <input type="submit" name="comment" value="Comment" class="btn btn-primary">    
                </form> 
              </div>

              @endforeach


              @else
              @endif
            </div>
          </div>
        </div>
        @endsection
