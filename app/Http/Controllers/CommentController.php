<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\CommentNotification;  
use App\Comment;
use App\Post;
use App\User;

use Thread;

class CommentController extends Controller
{
  protected $comments, $posts, $users;

  public function __construct(Comment $comments, Post $posts, User $users)
  {
    $this->comments = $comments;
    $this->posts = $posts;
    $this->users = $users;
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {

      $comment = new Comment([
        'text' => $request->get('text'),
        'user_id' => $request->get('user_id'),
        'post_id' => $request->get('post_id')
      ]);

      $post = $this->posts->findOrFail($request->get('post_id'));

      // $user = $this->users->findOrFail($request->get('user_id'));


      $user = $post->user;
      // dd($user);

      $user->notify(new CommentNotification( $post ));

        // dd($user);


        // dd('Hello Notification!');

      $comment->save();
      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  }
